package mostparts

import (
	"testing"

	"gitlab.com/octo42/mastermind/mastermind"
)

func TestBenchmark(t *testing.T) {
	results := mastermind.Benchmark(Strategy{})

	if got, want := results.TotalGuesses(), 5668; got != want {
		t.Errorf("TotalGuesses() = %d, want %d", got, want)
	}
	if got, want := results.MaxGuesses(), 6; got != want {
		t.Errorf("MaxGuesses() = %d, want %d", got, want)
	}
}
