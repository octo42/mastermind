package mastermind

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSolutionRate(t *testing.T) {
	cases := []struct {
		name          string
		guess, answer Code
		want          Hint
	}{
		{
			name:   "correct guess",
			guess:  Code{Green, Red, Orange, Blue},
			answer: Code{Green, Red, Orange, Blue},
			want:   Hint{4, 0},
		},
		{
			name:   "all colors correct",
			guess:  Code{Green, Red, Orange, Blue},
			answer: Code{Red, Orange, Blue, Green},
			want:   Hint{0, 4},
		},
		{
			name:   "all colors correct, some correct positions",
			guess:  Code{Green, Red, Orange, Blue},
			answer: Code{Green, Red, Blue, Orange},
			want:   Hint{2, 2},
		},
		{
			name:   "guess with repeating colors",
			guess:  Code{Green, Green, Red, Red},
			answer: Code{Green, Red, Blue, Orange},
			want:   Hint{1, 1},
		},
		{
			name:   "answer with repeating colors",
			guess:  Code{Green, Red, Blue, Orange},
			answer: Code{Green, Green, Red, Red},
			want:   Hint{1, 1},
		},
	}

	for _, tc := range cases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			got := tc.guess.Rate(tc.answer)
			if !got.Equal(tc.want) {
				t.Errorf("Rate(%v, %v) = %#v, want %#v", tc.guess, tc.answer, got, tc.want)
			}

			got = tc.guess.rateSlice(tc.answer)
			if !got.Equal(tc.want) {
				t.Errorf("rateSlice(%v, %v) = %#v, want %#v", tc.guess, tc.answer, got, tc.want)
			}
		})
	}
}

func TestAllSolutions(t *testing.T) {
	var n int
	ch := AllCodes(4, 6, Rules{AllowDuplicateColors: false})
	for s := range ch {
		t.Logf("s = %v", s)
		n++
	}
	if want := 6 * 5 * 4 * 3; n != want {
		t.Errorf("AllSolutions(4) returned %d Solutions, want %d", n, want)
	}
}

func TestSolutionSpace_Filter(t *testing.T) {
	cases := []struct {
		name  string
		input SolutionSpace
		code  Code
		hint  Hint
		want  SolutionSpace
	}{
		{
			name: "initial guess",
			input: SolutionSpace{
				Code{Red, Green, Blue, Yellow},
				Code{Red, Orange, Blue, Yellow},
				Code{Blue, Orange, Red, Green},
			},
			code: Code{Red, Red, Green, Green},
			hint: Hint{CorrectPosition: 1, IncorrectPosition: 1},
			want: SolutionSpace{
				Code{Red, Green, Blue, Yellow},
				Code{Blue, Orange, Red, Green},
			},
		},
		{
			name: "no remaining codes",
			input: SolutionSpace{
				Code{Red, Pink, Blue, Yellow},
				Code{Red, Orange, Blue, Yellow},
				Code{Blue, Orange, Red, Pink},
			},
			code: Code{Red, Red, Green, Green},
			hint: Hint{CorrectPosition: 1, IncorrectPosition: 1},
			want: nil,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			got := tc.input.Filter(tc.code, tc.hint)

			if diff := cmp.Diff(tc.want, got); diff != "" {
				t.Errorf("SolutionSpace.Filter() differs (-want/+got):\n%s", diff)
			}
		})
	}
}

func TestSolutionSpace_Partition(t *testing.T) {
	cases := []struct {
		code  Code
		num   int
		worst int
	}{
		{
			code:  Code{0, 0, 0, 0},
			num:   5,
			worst: 625,
		},
		{
			code:  Code{0, 0, 0, 1},
			num:   11,
			worst: 317,
		},
		{
			code:  Code{0, 0, 1, 1},
			num:   13,
			worst: 256,
		},
		{
			code:  Code{0, 0, 1, 2},
			num:   14,
			worst: 276,
		},
		{
			code:  Code{0, 1, 2, 3},
			num:   14,
			worst: 312,
		},
	}

	for _, tc := range cases {
		t.Run(tc.code.String(), func(t *testing.T) {
			ss := AllCodes(4, 6, Rules{AllowDuplicateColors: true})
			histogram := ss.Partition(tc.code)

			num := len(histogram)
			var worst int
			for _, n := range histogram {
				if worst < n {
					worst = n
				}
			}

			if got, want := num, tc.num; got != want {
				t.Errorf("len(Partition()) = %d, want %d", got, want)
			}
			if got, want := worst, tc.worst; got != want {
				t.Errorf("max(Partition()) = %d, want %d", got, want)
			}
		})
	}
}
