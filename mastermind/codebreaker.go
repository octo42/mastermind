package mastermind

import (
	"errors"
	"fmt"
)

// Oracle is used by CodeBreaker to issue a guess and retrieve a hint.
type Oracle interface {
	Rate(Code) (Hint, error)
}

// Strategy is used by CodeBreaker to find the correct code.
// The remaining solution space is passed into the Strategy, and the next guess is returned.
type Strategy interface {
	NextGuess(ss SolutionSpace, guessCount, numColors int) Code
}

// CodeBreaker implements generic code for solving a Mastermind game.
type CodeBreaker struct {
	numSlots  int
	numColors int
	rules     Rules
	strategy  Strategy
}

// NewCodeBreaker initializes and returns a new CodeBreaker.
func NewCodeBreaker(numSlots, numColors int, rules Rules, strategy Strategy) CodeBreaker {
	return CodeBreaker{
		numSlots:  numSlots,
		numColors: numColors,
		rules:     rules,
		strategy:  strategy,
	}
}

// Solve finds the correct code by getting the next guess from the Strategy and having it evaluated by the Oracle.
// Returns the number of guesses needed. Returns 9 if the correct code could not be guessed.
func (cb CodeBreaker) Solve(oracle Oracle) ([]Code, error) {
	var (
		possibleCodes = AllCodes(cb.numSlots, cb.numColors, cb.rules)
		counter       int
		guesses       []Code
	)

	for len(possibleCodes) > 0 {
		counter++
		if counter >= 9 {
			fmt.Printf("😭 We lost! 😭\n\n")
			return nil, errors.New("unable to break the code")
		}

		fmt.Printf("Guess #%d: There are %d possible codes left.\n", counter, len(possibleCodes))

		guess := cb.strategy.NextGuess(possibleCodes, counter, cb.numColors)
		guesses = append(guesses, guess)
		fmt.Printf("Guess #%d: Let's try %v\n", counter, guess)

		hint, err := oracle.Rate(guess)
		if err != nil {
			return nil, err
		}

		fmt.Printf("Guess #%d: got hint %v\n", counter, hint)
		if hint.CorrectPosition == cb.numSlots {
			break
		}

		possibleCodes = possibleCodes.Filter(guess, hint)
	}

	fmt.Printf("🎉 Finished after %d guesses 🎉\n\n", len(guesses))
	return guesses, nil
}
