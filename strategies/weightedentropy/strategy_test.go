package weightedentropy

import (
	"testing"

	"gitlab.com/octo42/mastermind/mastermind"
)

func TestBenchmark(t *testing.T) {
	results := mastermind.Benchmark(Strategy{
		Coefficients: GurCoefficients,
	})

	if got, want := results.TotalGuesses(), 5646; got != want {
		t.Errorf("TotalGuesses() = %d, want %d", got, want)
	}
	if got, want := results.MaxGuesses(), 6; got != want {
		t.Errorf("MaxGuesses() = %d, want %d", got, want)
	}
	// Gur's website claims "3", but their published code requires six guesses for:
	// <2,4,6,6>, <3,6,1,4>, <4,2,5,3>, <5,4,4,3>, and <6,4,6,2>.
	if got, want := results.NUp(6), 5; got != want {
		t.Errorf("NUp(6) = %d, want %d", got, want)
	}
}

func TestScore(t *testing.T) {
	// histogram for <1,2,3,4>
	histogram := map[mastermind.Hint]int{
		{CorrectPosition: 0, IncorrectPosition: 0}: 16,
		{CorrectPosition: 0, IncorrectPosition: 1}: 152,
		{CorrectPosition: 0, IncorrectPosition: 2}: 312,
		{CorrectPosition: 0, IncorrectPosition: 3}: 136,
		{CorrectPosition: 0, IncorrectPosition: 4}: 9,
		{CorrectPosition: 1, IncorrectPosition: 0}: 108,
		{CorrectPosition: 1, IncorrectPosition: 1}: 252,
		{CorrectPosition: 1, IncorrectPosition: 2}: 132,
		{CorrectPosition: 1, IncorrectPosition: 3}: 8,
		{CorrectPosition: 2, IncorrectPosition: 0}: 96,
		{CorrectPosition: 2, IncorrectPosition: 1}: 48,
		{CorrectPosition: 2, IncorrectPosition: 2}: 6,
		{CorrectPosition: 3, IncorrectPosition: 0}: 20,
		{CorrectPosition: 4, IncorrectPosition: 0}: 1,
	}

	s := Strategy{
		Coefficients: GurCoefficients,
	}

	got := s.score(histogram, 1296)
	want := 2950580

	if got != want {
		t.Errorf("score() = %d, want %d", got, want)
	}
}
