# mastermind

This is an implementatio of the board game
[Mastermind](https://en.wikipedia.org/wiki/Mastermind_(board_game)) in Go.

At the moment, this consists of a Go package implementing the game logic. There
is also a solver which guesses one of the remaining possible codes at random.
The goal is to implement an optimal solver for the game.

It should be fairly straightforward to implement a CLI or web-based version of
the game based on the Go packags, but this is not currently planned.

## License

This work is published under the
[ISC License](https://opensource.org/license/isc-license-txt/).

## Author

Florian Forster &lt;ff at octo.it&gt;