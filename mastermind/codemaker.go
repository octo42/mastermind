package mastermind

import "errors"

// CodeMaker implements the side of the game accepting guesses and issuing hints.
type CodeMaker struct {
	codes chan Code
	hints chan Hint
}

// RandomCodeMaker starts a new game with a random code.
// See NewCodeMaker for details.
func RandomCodeMaker(numSlots, numColors int, rules Rules) CodeMaker {
	code := RandomCode(numSlots, numColors, rules)
	return NewCodeMaker(code)
}

// NewCodeMaker starts a new game, which runs as a separate Goroutine.
//
// Once the correct code is guessed and success is signaled by returning a
// "fully correct" Hint, the Hint channel is closed and the Goroutine exits.
//
// To abort a game early, close the Code channel.
func NewCodeMaker(code Code) CodeMaker {
	codes := make(chan Code)
	hints := make(chan Hint)

	cm := CodeMaker{
		codes: codes,
		hints: hints,
	}
	go cm.serveGame(code)

	return cm
}

// Stop ends a game early, i.e. before the correct code has been guessed.
func (cm CodeMaker) Stop() {
	select {
	case <-cm.codes:
		// closed
	default:
		close(cm.codes)
	}
}

// Rate compares code with the to-be-guessed code and returns a hint.
// If the guess is correct, the background Goroutine exits and no further guesses are possible.
//
// Rate implements the Oracle interface.
func (cm CodeMaker) Rate(code Code) (Hint, error) {
	cm.codes <- code
	hint, ok := <-cm.hints
	if !ok {
		return Hint{}, errors.New("hints channel is closed")
	}
	return hint, nil
}

func (cm CodeMaker) serveGame(goal Code) {
	numSlots := len(goal)
	for s := range cm.codes {
		hint := goal.Rate(s)
		cm.hints <- hint
		if hint.CorrectPosition == numSlots {
			break
		}
	}

	close(cm.hints)
}
