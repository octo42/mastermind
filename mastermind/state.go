package mastermind

import (
	"fmt"
	"math/rand"
	"slices"
	"strings"
)

// Color encodes the alphabet of the game's Code.
type Color int

const (
	Green Color = iota
	Red
	Orange
	Blue
	Yellow
	Pink
)

// Hint provides information about the relationship of two codes.
//
// For each Color that is in the same location in both codes, CorrectPosition
// is increased by one. This is equivalent to black pegs in the board game.
// For remaining Colors that are in both codes but at different locations,
// IncorrectPosition is increased by one.
type Hint struct {
	// CorrectPosition counts the Colors that appear in both codes in the same location.
	// The board game typically uses black pegs for this.
	CorrectPosition int

	// IncorrectPosition counts the Colors that appear in both codes, but in different locations.
	// The board game typically uses white pegs for this.
	IncorrectPosition int
}

// Equal returns true if both Hints are identical.
func (h Hint) Equal(other Hint) bool {
	return h.CorrectPosition == other.CorrectPosition && h.IncorrectPosition == other.IncorrectPosition
}

func (h Hint) String() string {
	return fmt.Sprintf("[%d black, %d white]", h.CorrectPosition, h.IncorrectPosition)
}

// Code is a list of Colors.
type Code []Color

func (c Code) String() string {
	var buf strings.Builder
	for i, color := range c {
		if i == 0 {
			fmt.Fprintf(&buf, "<%v", color+1)
		} else {
			fmt.Fprintf(&buf, ",%v", color+1)
		}
	}
	buf.WriteRune('>')
	return buf.String()
}

func (c Code) Clone() Code {
	cpy := make(Code, len(c))
	copy(cpy, c)
	return cpy
}

func (c Code) Equal(c2 Code) bool {
	return slices.Equal(c, c2)
}

// Compare returns:
// * 0 (zero) if c and c2 are equal
// * 1 (one) if c is greater than c2
// * -1 (minus one) if c is less than c2
func (c Code) Compare(c2 Code) int {
	return slices.Compare(c, c2)
}

func (c Code) Before(c2 Code) bool {
	return c.Compare(c2) < 0
}

func (c Code) After(c2 Code) bool {
	return c.Compare(c2) > 0
}

// Rate compares c and other and returns a Hint indicating matches in color and position, and
// matches in color only.
func (c Code) Rate(other Code) Hint {
	// This is a "hot" function, accounting for ~70% of a benchmark's runtime.
	// The implementation below uses a bitmap to avoid slice allocations, resulting in 1.75x
	// speedup. Larger codes use the slower slice-based implementation.
	if len(c) > 64 {
		return c.rateSlice(other)
	}

	var (
		doneC, doneOther uint64
		h                Hint
	)

	// check for colors in the correct position first.
	for i, color := range c {
		if other[i] != color {
			continue
		}

		h.CorrectPosition++
		doneC |= 1 << i
		doneOther |= 1 << i
	}

	for i, color0 := range c {
		if doneC&(1<<i) != 0 {
			continue
		}

		for j, color1 := range other {
			if doneOther&(1<<j) != 0 || color0 != color1 {
				continue
			}
			h.IncorrectPosition++
			doneC |= 1 << i
			doneOther |= 1 << j
			break
		}
	}

	return h
}

func (c Code) rateSlice(other Code) Hint {
	doneC := make([]bool, len(c))
	doneOther := make([]bool, len(other))

	var h Hint

	// check for colors in the correct position first.
	for i, color := range c {
		if other[i] != color {
			continue
		}

		h.CorrectPosition++
		doneC[i] = true
		doneOther[i] = true
	}

	for i, color0 := range c {
		if doneC[i] {
			continue
		}

		for j, color1 := range other {
			if doneOther[j] || color0 != color1 {
				continue
			}
			h.IncorrectPosition++
			doneC[i] = true
			doneOther[j] = true
			break
		}
	}

	return h
}

// SolutionSpace represents the set of codes satisfying all previous hints in the game.
type SolutionSpace []Code

// AllCodes returns a sorted list of all possible codes.
func AllCodes(numSlots, numColors int, rules Rules) SolutionSpace {
	return allCodes(numSlots, Code{}, makeColors(numColors), rules)
}

func allCodes(remainingSlots int, partialCode Code, availableColors []Color, rules Rules) []Code {
	if remainingSlots == 0 {
		return SolutionSpace{partialCode}
	}

	var ret SolutionSpace
	for i, color := range availableColors {
		pc := partialCode.Clone()
		pc = append(pc, color)

		nextAvailableColors := availableColors
		if !rules.AllowDuplicateColors {
			nextAvailableColors = slices.Delete(slices.Clone(availableColors), i, i+1)
		}

		codes := allCodes(remainingSlots-1, pc, nextAvailableColors, rules)
		ret = append(ret, codes...)
	}

	return ret
}

type Rules struct {
	AllowDuplicateColors bool
}

// Filter returns a new SolutionSpace with only the Codes that produce the same hint for the given code.
// Additionally, the passed in code is also filtered out.
// This function retains the original order.
func (ss SolutionSpace) Filter(code Code, hint Hint) SolutionSpace {
	var ret SolutionSpace
	for _, s := range ss {
		h := code.Rate(s)
		if hint.Equal(h) && !code.Equal(s) {
			ret = append(ret, s)
		}
	}
	return ret
}

// Partition partitions the solution space depending on what hint elements of ss would get if
// compared to code.
func (ss SolutionSpace) Partition(code Code) map[Hint]int {
	ret := make(map[Hint]int)
	for _, s := range ss {
		hint := code.Rate(s)
		ret[hint]++
	}
	return ret
}

// Contains returns true if ss contains c.
// This function uses a binary search and therefore assumes that ss is sorted.
func (ss SolutionSpace) Contains(c Code) bool {
	_, ok := slices.BinarySearchFunc(ss, c, func(e, t Code) int {
		return e.Compare(t)
	})
	return ok
}

func (ss SolutionSpace) PickRandom() Code {
	idx := rand.Intn(len(ss))
	return ss[idx]
}

// AllHints returns all possible hints for the given number of slots.
func AllHints(numSlots int) []Hint {
	var ret []Hint
	for black := 0; black <= numSlots; black++ {
		for white := 0; black+white <= numSlots; white++ {
			if black+white == numSlots && white == 1 {
				continue
			}
			ret = append(ret, Hint{
				CorrectPosition:   black,
				IncorrectPosition: white,
			})
		}
	}
	return ret
}

// RandomCode returns a random Code.
func RandomCode(numSlots, numColors int, rules Rules) Code {
	colors := makeColors(numColors)

	var c Code
	for i := 0; i < numSlots; i++ {
		idx := rand.Intn(len(colors))
		c = append(c, colors[idx])
		if !rules.AllowDuplicateColors {
			colors = slices.Delete(colors, idx, idx+1)
		}
	}
	return c
}

func makeColors(n int) []Color {
	colors := make([]Color, n)
	for i := 0; i < n; i++ {
		colors[i] = Color(i)
	}
	return colors
}
