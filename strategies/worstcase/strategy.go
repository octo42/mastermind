// Package worstcase implements the "worst case" strategy described by Donald Knuth in
// their 1976–1977 paper "The Computer as Master Mind".
package worstcase

import (
	"fmt"

	"gitlab.com/octo42/mastermind/mastermind"
)

type Strategy struct{}

// NextGuess returns the code which eliminates the most Codes from the solution space ss.
// Only considering codes that are in ss is not optimal and this function may return codes not in ss.
func (Strategy) NextGuess(ss mastermind.SolutionSpace, guessCount, numColors int) mastermind.Code {
	if len(ss) <= 2 {
		return ss[0]
	}

	numSlots := len(ss[0])
	if guessCount == 1 && numSlots == 4 {
		return mastermind.Code{0, 0, 1, 1}
	}

	var (
		minMaxRemaining = len(ss)
		retCode         mastermind.Code
		inSolutionSpace bool

		allCodes = mastermind.AllCodes(numSlots, numColors, mastermind.Rules{AllowDuplicateColors: true})
	)

	for _, code := range allCodes {
		var (
			histogram    = ss.Partition(code)
			maxRemaining int
			worstHint    mastermind.Hint
		)
		for hint, remaining := range histogram {
			if maxRemaining < remaining {
				maxRemaining = remaining
				worstHint = hint
			}
		}

		if minMaxRemaining > maxRemaining ||
			(minMaxRemaining == maxRemaining && !inSolutionSpace && ss.Contains(code)) {
			fmt.Printf("    [worstcase] code %v (from soluton space: %v) leaves at most %d solution(s) with hint %v\n",
				code, ss.Contains(code), maxRemaining, worstHint)
			minMaxRemaining = maxRemaining
			retCode = code.Clone()
			inSolutionSpace = ss.Contains(code)
		}
	}

	return retCode
}
