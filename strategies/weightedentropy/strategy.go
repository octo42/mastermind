// Package weightedentropy implements the "weighted entropy" stragety publicized by Serkan Gur in 2021.
package weightedentropy

import (
	"fmt"
	"math"

	"gitlab.com/octo42/mastermind/mastermind"
)

type Strategy struct {
	Coefficients map[mastermind.Hint]float64
}

// NextGuess returns the code which partitions the remaining solution space the most.
func (s Strategy) NextGuess(ss mastermind.SolutionSpace, guessCount, numColors int) mastermind.Code {
	if len(ss) <= 2 {
		return ss[0]
	}

	numSlots := len(ss[0])
	if guessCount == 1 && numSlots == 4 && numColors == 6 {
		return mastermind.Code{0, 0, 1, 2}
	}

	var (
		highScore int
		retCode   mastermind.Code

		allCodes = mastermind.AllCodes(numSlots, numColors, mastermind.Rules{AllowDuplicateColors: true})
	)

	for _, code := range allCodes {
		histogram := ss.Partition(code)

		score := s.score(histogram, len(ss))
		if highScore < score {
			fmt.Printf("    [weighted entropy] code %v (from soluton space: %v) has score %d\n",
				code, ss.Contains(code), score)
		}
		if highScore < score {
			retCode = code
			highScore = score
		}
	}

	return retCode
}

func (s Strategy) score(histogram map[mastermind.Hint]int, numCodes int) int {
	var score float64
	for hint, count := range histogram {
		propability := float64(count) / float64(numCodes)
		entropy := -1.0 * propability * math.Log2(propability)

		score += entropy * s.coefficient(hint)
	}

	return int(math.Round(score * 1000000.0))
}

var GurCoefficients = map[mastermind.Hint]float64{
	{CorrectPosition: 0, IncorrectPosition: 0}: 1.10,
	{CorrectPosition: 0, IncorrectPosition: 1}: 1.07,
	{CorrectPosition: 0, IncorrectPosition: 2}: 1.04,
	{CorrectPosition: 0, IncorrectPosition: 3}: 0.81,
	{CorrectPosition: 0, IncorrectPosition: 4}: 0.62,
	{CorrectPosition: 1, IncorrectPosition: 0}: 1.02,
	{CorrectPosition: 1, IncorrectPosition: 1}: 0.95,
	{CorrectPosition: 1, IncorrectPosition: 2}: 1.05,
	{CorrectPosition: 1, IncorrectPosition: 3}: 0.88,
	{CorrectPosition: 2, IncorrectPosition: 0}: 0.92,
	{CorrectPosition: 2, IncorrectPosition: 1}: 0.79,
	{CorrectPosition: 2, IncorrectPosition: 2}: 1.02,
	{CorrectPosition: 3, IncorrectPosition: 0}: 0.80,
	{CorrectPosition: 4, IncorrectPosition: 0}: 1.93,
}

// OctoCoefficients solves a standard Mastermind benchmark with 5654 guesses and without exceeding
// five guesses.
var OctoCoefficients = map[mastermind.Hint]float64{
	{CorrectPosition: 0, IncorrectPosition: 0}: 1.07,
	{CorrectPosition: 0, IncorrectPosition: 1}: 0.99,
	{CorrectPosition: 0, IncorrectPosition: 2}: 0.87,
	{CorrectPosition: 0, IncorrectPosition: 3}: 0.78,
	{CorrectPosition: 0, IncorrectPosition: 4}: 0.93,
	{CorrectPosition: 1, IncorrectPosition: 0}: 1.11,
	{CorrectPosition: 1, IncorrectPosition: 1}: 0.98,
	{CorrectPosition: 1, IncorrectPosition: 2}: 0.90,
	{CorrectPosition: 1, IncorrectPosition: 3}: 1.20,
	{CorrectPosition: 2, IncorrectPosition: 0}: 0.94,
	{CorrectPosition: 2, IncorrectPosition: 1}: 0.74,
	{CorrectPosition: 2, IncorrectPosition: 2}: 0.98,
	{CorrectPosition: 3, IncorrectPosition: 0}: 0.84,
	{CorrectPosition: 4, IncorrectPosition: 0}: 1.68,
}

func (s Strategy) coefficient(hint mastermind.Hint) float64 {
	if len(s.Coefficients) == 0 {
		return 1.0
	}
	if c, ok := s.Coefficients[hint]; ok {
		return c
	}
	return 1.0
}
