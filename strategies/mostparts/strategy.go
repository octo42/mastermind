// Package mostparts implements the "most parts" described by Barteld Kooi in
// their 2005 paper "Yet another mastermind strategy".
package mostparts

import (
	"fmt"

	"gitlab.com/octo42/mastermind/mastermind"
)

type Strategy struct{}

// NextGuess returns the code which partitions the remaining solution space the most.
func (Strategy) NextGuess(ss mastermind.SolutionSpace, guessCount, numColors int) mastermind.Code {
	if len(ss) <= 2 {
		return ss[0]
	}

	numSlots := len(ss[0])
	if guessCount == 1 && numSlots == 4 {
		return mastermind.Code{0, 0, 1, 2}
	}

	var (
		numParts        int
		retCode         mastermind.Code
		inSolutionSpace bool

		allCodes = mastermind.AllCodes(numSlots, numColors, mastermind.Rules{AllowDuplicateColors: true})
	)

	for _, code := range allCodes {
		histogram := ss.Partition(code)

		if np := len(histogram); numParts < np || (numParts == np && !inSolutionSpace && ss.Contains(code)) {
			fmt.Printf("    [mostparts] code %v (from soluton space: %v) creates %d partitions\n",
				code, ss.Contains(code), np)
			numParts = np
			retCode = code.Clone()
			inSolutionSpace = ss.Contains(code)
		}
	}

	return retCode
}
