package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"

	"gitlab.com/octo42/mastermind/mastermind"
	"gitlab.com/octo42/mastermind/strategies/mostparts"
	"gitlab.com/octo42/mastermind/strategies/weightedentropy"
	"gitlab.com/octo42/mastermind/strategies/worstcase"
)

var (
	strategy mastermind.Strategy = worstcase.Strategy{}

	profile = flag.String("profile", "", "output file of the CPU profile")
)

func main() {
	flag.Func("strategy", "set the strategy to benchmark. Valid values are: worstcase, mostparts, weightedentropy", setStrategy)
	flag.Parse()

	if *profile != "" {
		f, err := os.Create(*profile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	res := mastermind.Benchmark(strategy)

	fmt.Println("================================================================================")
	fmt.Printf("Total guesses:     %5d\n", res.TotalGuesses())
	fmt.Printf("Max guesses:       %5d\n", res.MaxGuesses())
	fmt.Printf("6 or more guesses: %5d\n", res.NUp(6))
	fmt.Println("================================================================================")
}

func setStrategy(name string) error {
	switch name {
	case "worstcase":
		strategy = worstcase.Strategy{}
	case "mostparts":
		strategy = mostparts.Strategy{}
	case "weightedentropy":
		strategy = weightedentropy.Strategy{
			Coefficients: weightedentropy.OctoCoefficients,
		}
	default:
		return fmt.Errorf("invalid strategy: %q", name)
	}
	return nil
}
