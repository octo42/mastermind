// solver implements a solver for Mastermind games.
//
// It can utilize different strategies which are implemented in the strategies/ directory.
package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/octo42/mastermind/mastermind"
	"gitlab.com/octo42/mastermind/strategies/mostparts"
	"gitlab.com/octo42/mastermind/strategies/weightedentropy"
	"gitlab.com/octo42/mastermind/strategies/worstcase"
)

var (
	strategy mastermind.Strategy = worstcase.Strategy{}

	numColors = flag.Int("colors_num", 6, "The overall number of colors in the game.")
	numSlots  = flag.Int("slots_num", 4, "The number of colors in each code.")
)

var rules = mastermind.Rules{
	AllowDuplicateColors: true,
}

func main() {
	flag.BoolVar(&rules.AllowDuplicateColors, "duplicates", true, "Whether the solution may contain duplicate colors")
	flag.Func("strategy", "set the strategy to benchmark. Valid values are: worstcase, mostparts, weightedentropy", setStrategy)
	flag.Parse()

	fmt.Println("Solving a Mastermind game with", *numSlots, "slots and ", *numColors, "colors")

	cb := mastermind.NewCodeBreaker(*numSlots, *numColors, rules, strategy)
	cm := &codeMaker{
		reader: bufio.NewReader(os.Stdin),
	}

	_, err := cb.Solve(cm)
	if err != nil {
		log.Fatal(err)
	}
}

type codeMaker struct {
	counter int
	reader  *bufio.Reader
}

func (cm *codeMaker) Rate(code mastermind.Code) (mastermind.Hint, error) {
	cm.counter++
	fmt.Printf("Guess #%d: %v\n", cm.counter, code)

	return readHint(cm.reader)
}

func readHint(in *bufio.Reader) (mastermind.Hint, error) {
	const errMsg = `Please enter a hint as [black,white], for example "4,0" for the winning move.`
	for {
		fmt.Printf("Enter hint: ")
		ns, err := readIntSlice(in)
		if err != nil {
			fmt.Println(err)
			fmt.Println(errMsg)
			continue
		}
		if len(ns) != 2 {
			fmt.Println(errMsg)
			continue
		}

		return mastermind.Hint{
			CorrectPosition:   ns[0],
			IncorrectPosition: ns[1],
		}, nil
	}
}

func readIntSlice(in *bufio.Reader) ([]int, error) {
	s, err := in.ReadString('\n')
	if err != nil {
		return nil, err
	}
	s = strings.TrimSpace(s)

	ss := strings.Split(s, ",")
	if len(ss) == 0 {
		return nil, errors.New("invalid empty input")
	}

	ret := make([]int, len(ss))
	for i := range ss {
		n, err := strconv.Atoi(ss[i])
		if err != nil {
			return nil, err
		}
		ret[i] = n
	}

	return ret, nil
}

func setStrategy(name string) error {
	switch name {
	case "worstcase":
		strategy = worstcase.Strategy{}
	case "mostparts":
		strategy = mostparts.Strategy{}
	case "weightedentropy":
		strategy = weightedentropy.Strategy{
			Coefficients: weightedentropy.GurCoefficients,
		}
	default:
		return fmt.Errorf("invalid strategy: %q", name)
	}
	return nil
}
