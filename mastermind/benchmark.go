package mastermind

import (
	"fmt"
	"os"
	"runtime"
	"sync"
)

// Benchmark runs strategy against all possible four slots, six color codes.
func Benchmark(strategy Strategy) BenchmarkResult {
	const (
		numSlots  = 4
		numColors = 6
	)
	rules := Rules{AllowDuplicateColors: true}

	results := make(BenchmarkResult)

	output := make(chan int)
	accumulator := sync.WaitGroup{}
	accumulator.Add(1)
	go func() {
		defer accumulator.Done()
		for n := range output {
			results[n]++
		}
	}()

	input := make(chan Code)
	workers := sync.WaitGroup{}
	for i := 0; i < 2*runtime.NumCPU(); i++ {
		workers.Add(1)
		go func() {
			defer workers.Done()
			for code := range input {
				cm := NewCodeMaker(code)
				cb := NewCodeBreaker(numSlots, numColors, rules, strategy)

				guesses, err := cb.Solve(cm)
				if err != nil {
					fmt.Fprintln(os.Stderr, err)
					continue
				}
				output <- len(guesses)
			}
		}()
	}

	allCodes := AllCodes(numSlots, numColors, rules)
	for _, code := range allCodes {
		input <- code
	}
	close(input)

	workers.Wait()
	close(output)

	accumulator.Wait()
	return results
}

// BenchmarkResult holds the results of a benchmark run.
// It is a map mapping number of guesses to number of occurrences.
type BenchmarkResult map[int]int

// TotalGuesses returns the total number of guesses required by the benchmark run.
func (r BenchmarkResult) TotalGuesses() int {
	var sum int
	for guesses, count := range r {
		sum += guesses * count
	}
	return sum
}

// MaxGuesses returns the maximum number of guesses required to correctly guess a code.
func (r BenchmarkResult) MaxGuesses() int {
	var max int
	for guesses := range r {
		if max < guesses {
			max = guesses
		}
	}
	return max
}

// NUp returns the number of codes that required n or more guesses.
func (r BenchmarkResult) NUp(n int) int {
	var sum int
	for guesses, count := range r {
		if guesses >= n {
			sum += count
		}
	}
	return sum
}
